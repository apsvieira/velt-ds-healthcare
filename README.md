## How to read this

This repository contains analysis notebooks, and some data files and documentations.

- Start by the 'notebooks/07_analise_empresas.ipynb' notebook.
- The remaining notebooks, while not as well presented, contain details on different segments of the analysis, and can be navigated by their filenames.
- The data folder contains datasets, documentation and images used.